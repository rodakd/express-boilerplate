const { validate } = require('../../services/validator.service');

const signup = (req, res, next) => {
  const validationRule = {
    email: 'required|email',
    password: 'required|string|min:6',
  };
  validate(req.body, validationRule, res, next);
};

const signin = (req, res, next) => {
  const validationRule = {
    email: 'required|email',
    password: 'required|string',
  };
  validate(req.body, validationRule, res, next);
};

module.exports = { signup, signin };
