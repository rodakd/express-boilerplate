const Validator = require('validatorjs');

const validator = (body, rules, customMessages, callback) => {
  const validation = new Validator(body, rules, customMessages);
  validation.passes(() => callback(null, true));
  validation.fails(() => callback(validation.errors, false));
};

const validate = (body, rule, res, next) =>
  validator(
    body,
    rule,
    {
      min: {
        numeric: 'Minimalna liczba :min',
        string: 'Minimalna liczba znaków :min',
      },
      max: {
        numeric: 'Maksymalna liczba :max.',
        string: 'Maksymalna liczba znaków: :max.',
      },
      string: 'Pole musi byc tekstem',
      required: 'Pole jest wymagane',
      numeric: 'Pole musi być liczbą',
      confirmed: 'Hasła muszą zgadzać się ze sobą',
      email: 'Błędny adres email',
      date: 'Błędy format daty',
      regex: 'Format jest nieprawdidłowy',
    },
    (err, status) => {
      if (!status) {
        res.status(412).send({
          success: false,
          message: 'Validation failed',
          data: err,
        });
      } else {
        next();
      }
    }
  );

module.exports = { validator, validate };
