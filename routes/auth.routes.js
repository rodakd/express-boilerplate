const express = require('express');
const { authValidation } = require('../middleware');
const { authController } = require('../controllers');

const router = express.Router();

router.post('/api/auth/signup', authValidation.signup, authController.signup);
router.post('/api/auth/signin', authValidation.signin, authController.signin);

module.exports = router;
