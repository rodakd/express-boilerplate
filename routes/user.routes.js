const express = require('express');
const { userController } = require('../controllers');

const router = express.Router();

router.get('/api/users', userController.getAllUsers);
router.get('/api/user/:id', userController.getProfile);

module.exports = router;
