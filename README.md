# basic-boilerplate

Minimalny setup API z podstawową rejestracją, logowaniem, wyświetlaniem użytkowników.

Zawiera:

- wygenerowany .gitignore
- bcrypt
- express
- dotenv
- validatorjs
- mysql2
- sequelize
- handlebars
- cors
