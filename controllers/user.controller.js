const db = require('../models');

const { User } = db;

exports.getProfile = (req, res) => {
  const userId = req.params.id;
  User.findOne({
    attributes: ['email'],
    where: {
      id: userId,
    },
  })
    .then((user) => {
      res.status(200).render('profile', { id: userId, email: user.dataValues.email });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.getAllUsers = (req, res) => {
  User.findAll({
    attributes: ['email'],
  })
    .then((users) => {
      const emails = users.map((user) => user.dataValues.email);
      res.status(200).render('users', { emails });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};
