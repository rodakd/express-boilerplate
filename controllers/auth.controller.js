const db = require('../models');
const bcrypt = require('bcryptjs');

const User = db.User;

exports.signup = (req, res) => {
  User.create({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
  })
    .then(() => {
      res.send('Uzytkownik zarejestrowany pomyslnie');
    })
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};

exports.signin = (req, res) => {
  User.findOne({
    where: {
      email: req.body.email,
    },
  })
    .then(async (user) => {
      if (!user) {
        return res.status(404).send({ message: 'Błędne hasło lub email' });
      }

      var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: 'Błędne hasło lub email',
        });
      }

      res.status(200).send({
        accessToken: 'token', // todo
      });
    })
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};
